#!/usr/bin/env python
# -*- coding: utf8 -*-
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import os
import sys
import threading

class util:

    imgPath = "/home/h2a/Documentos/h2a/data/img/"
    bootPath = "/home/h2a/Documentos/h2a/data/bootloaders/atmega328p/"

    def worker(self, parent):
        self.parent = parent
        self.parent.btnAh.setEnabled(False)
        print 'Trabajando con la carga tras bambalinas'
        rep = os.system("avrdude  -c dragon_isp -p atmega328p -P usb -b 115200 -U flash:w:%soptiboot_atmega328.hex -U lock:w:0x2f:m" % self.bootPath)
        if rep == 0:
            print 'Quemado satisfactoriamente'
            QtGui.QMessageBox.information(self.parent, "Informacion", """Carga terminada""",QtGui.QMessageBox.Ok)
        else:
            print 'No esta conectado nada no sirve pa nada XD'
        return

    def saludar(self, parent):
        '''
            parent: objeto del papa del hijo XD solo es para hacer
            referencia al objeto padre para poder digamos actualizar
            desde aca la interfaz por ejemplo
        '''
        self.parent = parent
        threads = list()
        print 'Si entre'
        t = threading.Thread(target=self.worker(self.parent))
        threads.append(t)
        t.start()
        #parent.lBD.setText('Hola')

    def center(self, parent):
        '''
            funcion para centrar la window en el centro del screen
        '''
        self.parent = parent
        frameGm = self.parent.frameGeometry()
        centerPoint = QtGui.QDesktopWidget().availableGeometry().center()
        frameGm.moveCenter(centerPoint)
        self.parent.move(frameGm.topLeft())

    def dragonCheck(self, parent):
        rep = os.system("avrdude  -c dragon_isp -p atmega328p -P usb -p m328p -F")
        if rep == 0:
            print 'Esta conectado'
            #QtGui.QMessageBox.information(self.parent, "Informacion", """AVRDragon conectado""",QtGui.QMessageBox.Ok)
        else:
            QtGui.QMessageBox.information(self.parent, "Informacion", """Favor de conectar el AVRDragon""",QtGui.QMessageBox.Ok)
            print 'No esta conectado nada no sirve pa nada XD'

    def checkPermiso(self, parent):
        '''
            funcion para centrar la window en el centro del screen
        '''
        self.parent = parent
        if os.geteuid() != 0:
            print 'Debes tener privilegios root para ejecutar este script.'
            QtGui.QMessageBox.information(self.parent, "Informacion", """Es necesario permiso\nde root para ejecutar""",QtGui.QMessageBox.Ok)
            sys.exit(1)
        else:
            print 'Bienvenido usuario root'
