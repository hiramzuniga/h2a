#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import inspect
from PyQt4 import QtGui
from PyQt4 import QtCore
import os
from time import strftime
import time
import threading
import util
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class init(QtGui.QMainWindow):
    def __init__(self):
        super(init, self).__init__()
        # Create an object of the class util
        funcion = util.util()
        funcion.checkPermiso(self)
        self.initUI(funcion)
        threads = list()
        t = threading.Thread(target=funcion.dragonCheck(self))
        threads.append(t)
        t.daemon = True
        t.start()
        #funcion.dragonCheck(self)

    def initUI(self, funcion):
        self.funcion = funcion
        self.saludo = QtGui.QLabel('Cargar Bootloader', self)
        self.saludo.resize(self.width(), 50)
        self.saludo.move(45, 5)
        self.saludo.setStyleSheet('QLabel { font-size: 15pt; font-family: \
            Arial; color:Orange}')
        self.btnAh = QtGui.QPushButton('', self)
        iconc = QIcon(self.funcion.imgPath+"ah.png")
        self.btnAh.setIconSize(QtCore.QSize(70, 70))
        self.btnAh.setIcon(iconc)
        self.btnAh.setStyleSheet("color:#293786;font-size: 0.1pt;font-weight: bold; background-color:#293786;")
        self.btnAh.move(90, 60)
        self.btnAh.setToolTip('Cargar bootloader <b>QPushButton</b> widget')
        self.btnAh.clicked[bool].connect(lambda: self.funcion.saludar(self))
        self.btnAh.resize(70, 70)
        #293786
        self.setGeometry(400, 300, 250, 150)
        self.setWindowTitle('Heart ToIno')
        self.setStyleSheet('''QMainWindow { background-color:#1D2556;}
            QLabel{font-size: 15pt; font-family: Courier; color:black;
            font-weight: bold}''')
        self.funcion.center(self)
        self.show()

def main():
    app = QtGui.QApplication(sys.argv)
    ex = init()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
